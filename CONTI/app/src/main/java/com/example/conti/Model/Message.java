package com.example.conti.Model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Message class, that every type of message class uses
 */
public class Message {
    public String senderId;
    public String receiverId;
    public String dateTime;
    public String type;
    public Object data;

    public Message(String senderId, String receiverId, String type, Object data)
    {
        this.senderId = senderId;
        this.receiverId = receiverId;
        makeTime();
        this.type = type;
        this.data = data;
    }

    public Message(){
        this.senderId = this.receiverId = this.type = this.dateTime = "";
        this.data = null;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    private void makeTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss aaa z");
        this.dateTime = simpleDateFormat.format(calendar.getTime()).toString();
    }
}
