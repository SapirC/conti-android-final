package com.example.conti;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.BoringLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.conti.Model.PictureDialog;
import com.example.conti.Model.User;

/**
 * A Settings Activity to change the profile picture (using the PictureDialog)
 * and changing the username
 */
public class SettingsActivity extends AppCompatActivity implements View.OnClickListener{

    private FBDatabase fb;
    private User user;

    private ImageView pfpHoldIv, pfpEditIv, backBtn;
    private EditText editUsername;

    private Button saveBtn;

    private PictureDialog pd;

    private Bitmap bmp;

    private Handler handler;

    private static final int IN_HANDLER = 2;

    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        fb = FBDatabase.getInstance(this);

        pfpHoldIv = (ImageView) findViewById(R.id.profileIv);

        pfpEditIv = (ImageView) findViewById(R.id.editPfpIv);
        pfpEditIv.setOnClickListener(this);

        backBtn = (ImageView) findViewById(R.id.backIv);
        backBtn.setOnClickListener(this);

        editUsername = (EditText) findViewById(R.id.editUsername);

        saveBtn = (Button) findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(this);

        fb.getUser(new Handler(handleFbData(this)));

        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.SAVE_DONE) {
                    if (msg.arg2 != FBDatabase.SAVE_OK)
                        Toast.makeText(SettingsActivity.this, "Save Failed", Toast.LENGTH_SHORT).show();
                    count ++;
                    //checking if everything has been saved and going back to the chats activity
                    if(count == IN_HANDLER) GoBack();
                }
                return true;
            }
        });

        pd = null;
    }

    /**
     * This function makes a handler for getting data from server
     * @param context Context
     * @return Handler.Callback
     */
    private Handler.Callback handleFbData(Context context){
        return new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {

                if (msg.arg1 == FBDatabase.READ_DONE) {
                    Intent intent = new Intent();
                    if(msg.arg2 == FBDatabase.READ_OK) {
                        user  =(User) msg.obj;
                        editUsername.setText(user.getUsername());
                        fb.retrievePfpUser(new Handler(fb.handleGetPfp(pfpHoldIv)),user.getKey());

                    }
                }
                return true;
            }
        };
    }

    public void GoBack(){
        Intent backIntent = new Intent(this, ChatsActivity.class);
        startActivity(backIntent);
        finish();
    }

    @Override
    public void onClick(View view) {
        if(view == pfpEditIv){
            pd= new PictureDialog(this);
            pd.show();
        }
        else if(view  == backBtn) {
            GoBack();
        }
        else if(view == saveBtn){
            //saving user data
            user.setUsername(editUsername.getText() + "");
            fb.saveUser(handler, user);
            //saving profile picture
            fb.uploadPfp(bmp, handler);

            //GoBack();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (pd != null) {
            bmp = pd.onActivityResult(requestCode, resultCode, data);

            //showing picture selected or taken
            pfpHoldIv.setImageBitmap(bmp);
        }
    }
}