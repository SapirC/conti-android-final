package com.example.conti;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.conti.Adapter.MessageAdapter;
import com.example.conti.Adapter.UserAdapter;
import com.example.conti.Model.LocationMessage;
import com.example.conti.Model.TextMessage;
import com.example.conti.Model.User;
import com.example.conti.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

/**
 * This Activity is the chat activity that displays the massages with the user you are talking to
 */
public class MessageActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    private FBDatabase fb;

    private User user;
    private User otherUser;

    private ImageView backIv, sendTextIv, sendLocationIv, pfpOtherUser;
    private EditText textSendEt;
    private TextView usernameOtherTv;

    private RecyclerView recyclerView;

    private MessageAdapter chatAdapter;
    private List<com.example.conti.Model.Message> mChat;

    private Handler mainHandler;
    private Pair<MapFragment, LatLng> pair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finishAffinity();
        }

        fb = FBDatabase.getInstance(this);

        //getting the current user
        fb.getUser(new Handler(handleGetCurrentUser()));

        //getting the other user
        Intent intent = getIntent();
        String userId = intent.getStringExtra("userKey");
        fb.getOtherUser(new Handler(handleGetOtherUser(this)), userId);


        backIv = (ImageView) findViewById(R.id.backIvChats);
        backIv.setOnClickListener(this);

        sendTextIv = (ImageView) findViewById(R.id.sendIv);
        sendTextIv.setOnClickListener(this);

        sendLocationIv = (ImageView) findViewById(R.id.locationSendIv);
        sendLocationIv.setOnClickListener(this);

        pfpOtherUser = (ImageView) findViewById(R.id.profileOtherIv);

        textSendEt = (EditText) findViewById(R.id.textSendEt);

        usernameOtherTv = (TextView) findViewById(R.id.usernameOtherTv);

        recyclerView = (RecyclerView) findViewById(R.id.rvChat);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mChat = new ArrayList<com.example.conti.Model.Message>();

        mainHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {

                pair.first.getMapAsync(MessageActivity.this);
            }
        };
    }

    /**
     * This function is updating the messages over the chat
     * getting the messages from the database
     */
    private void updateChat() {
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.MSGS_UPDATED) {
                    chatAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });
        fb.getMsgs(handler, mChat, otherUser.getKey(), recyclerView);
        recyclerView.scrollToPosition(chatAdapter.getItemCount()-1);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View view) {
        if (view == backIv) {
            fb.removeChatEvent();
            finish();
        } else if (view == sendTextIv) {
            String text = textSendEt.getText() + "";
            if (!text.equals("")) {
                com.example.conti.Model.Message msg = new TextMessage(user.getKey(), otherUser.getKey(),
                        text + "");
                sendMsg(msg);
            } else {
                Toast.makeText(this, "You can't send an empty message",
                        Toast.LENGTH_SHORT).show();
            }
        } else if (view == sendLocationIv) {
            FusedLocationProviderClient fusedLocationClient;
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            Log.d("sapir", fusedLocationClient + "");

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                //sending the location message
                                com.example.conti.Model.Message msg = new LocationMessage(user.getKey(), otherUser.getKey(),
                                        location);
                                sendMsg(msg);
                            }
                        }
                    });
        }
    }

    /**
     * This function is sending a message, saving in the database
     * and clearing the Message edit text
     * @param msg the message the user wants to send
     */
    private void sendMsg(com.example.conti.Model.Message msg){
        fb.saveMessage(null, msg);
        textSendEt.setText("");
    }

    /**
     * This function makes a handler for getting the other user
     * @return Handler.Callback
     */
    private Handler.Callback handleGetOtherUser(Context context){
        return new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                try{
                    if (msg.arg1 == FBDatabase.READ_DONE) {
                        if(msg.arg2 == FBDatabase.READ_OK) {
                            otherUser  = (User) msg.obj;
                            usernameOtherTv.setText(otherUser.getUsername());
                            //getting user's profile picture
                            fb.retrievePfpUser(new Handler(fb.handleGetPfp(pfpOtherUser)), otherUser.getKey());
                            chatAdapter = new MessageAdapter(context,
                                    mChat,
                                    user,
                                    otherUser,
                                    new Handler(handleGoogleMapMessages(context)));
                            recyclerView.setAdapter(chatAdapter);
                            updateChat();
                        }
                    }
                }
                catch (Exception e){
                    Log.d("sapir","ero:"+e.getMessage());
                }
                return true;
            }
        };
    }

    /**
     * This function makes a handler for getting Messages that need to display a google map
     * @return Handler.Callback
     */
    private Handler.Callback handleGoogleMapMessages(Context context){
        return new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == MessageAdapter.LOCATION_SENT) {
                    pair = (Pair<MapFragment, LatLng>) msg.obj;
                    // Get a handler that can be used to post to the main thread
                    Message msgMain = mainHandler.obtainMessage();
                    msgMain.arg1 = MessageAdapter.LOCATION_SENT;
                    mainHandler.sendMessage(msgMain);

                }
                return true;
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("sapir", "inside here");
        //setting location on map
        MarkerOptions markerOptions = new MarkerOptions().position(pair.second);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(pair.second));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pair.second, 5));
        googleMap.addMarker(markerOptions);
    }

    /**
     * This function makes a handler for getting the current user
     * @return Handler.Callback
     */
    private Handler.Callback handleGetCurrentUser(){
        return new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.READ_DONE) {
                    if(msg.arg2 == FBDatabase.READ_OK) {
                        user  = (User) msg.obj;
                    }
                }
                return true;
            }
        };
    }
}