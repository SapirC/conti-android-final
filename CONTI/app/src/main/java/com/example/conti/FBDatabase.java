package com.example.conti;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Message;

import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.conti.Adapter.UserAdapter;
import com.example.conti.Model.LocationMessage;
import com.example.conti.Model.TextMessage;
import com.example.conti.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class has all the relevant methods to make from the database - firebase (Singleton class)
 */
public class FBDatabase {
    private static Context context;
    private FirebaseDatabase appDatabase;
    private static FBDatabase myInstance = null;
    private DatabaseReference dbRef, userRef, usersRef, chatsRef;
    private FirebaseUser currentUser;
    private String uID;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private ValueEventListener usersEvent;
    private ValueEventListener chatEvent;

    private User user;
    private Bitmap pfp;

    public static final int SAVE_DONE = 0,
            SAVE_OK = 1,
            SAVE_ERROR = 2,
            READ_DONE = 3,
            READ_OK = 4,
            READ_ERROR = 5,
            NULL_USER = 6,
            DONE_UPLOAD_PIC = 7,
            DONE_RETRIEVED_PIC = 8,
            OK_RETRIEVED_PIC = 9,
            ERROR_RETRIEVED_PIC = 10,
            NULL_RETRIEVED_PIC = 11,
            USERS_UPDATED = 12,
            MSGS_UPDATED = 13;


    private FBDatabase() {
        dbRef = FirebaseDatabase.getInstance().getReference();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        appDatabase = FirebaseDatabase.getInstance();
        uID = currentUser.getUid();
        userRef = dbRef.child(uID);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        userRef = dbRef.child("Users").child(uID);
        usersRef = dbRef.child("Users");
        chatsRef = dbRef.child("Chats");
    }

    public static FBDatabase getInstance(Context context) {
        FBDatabase.context = context;
        if (myInstance == null)
            myInstance = new FBDatabase();
        return myInstance;
    }

    /**
     * The function is making a logout of the user from the Database
     */
    public void logout() {
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.SAVE_DONE && msg.arg2 == FBDatabase.SAVE_OK) {
                    FirebaseAuth.getInstance().signOut();
                    FBDatabase.myInstance = null;
                }
                return true;
            }
        });

        Handler handlerFinished = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.READ_DONE && msg.arg2 == FBDatabase.READ_OK) {
                    user.setLogged(false);
                    saveUser(handler, user);

                }
                return true;
            }}
        );

        this.getUser(handlerFinished);
    }

// --------------------------------------------------------------------------
//        FirebaseStorage
// ---------------------------------------------------------------------------

    /**
     * The function receive a User and a Handler and saving the User after changes
     *
     * @param data    User
     * @param handler Handler
     */
    public void saveUser(Handler handler, User data) {
        data.setKey(this.userRef.getKey());
        this.userRef.setValue(data, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                if(handler != null){
                    Message msg = handler.obtainMessage();
                    msg.arg1 = SAVE_DONE;
                    if (error == null) {
                        msg.arg2 = SAVE_OK;
                    } else {
                        msg.arg2 = SAVE_ERROR;
                        msg.obj = error.getMessage();
                    }
                    handler.sendMessage(msg);
                }
            }
        });
    }

    /**
     * The function is getting the current user and receiving an handler that uses this result
     *
     * @param handler Handler
     */
    public void getUser(Handler handler) {


        if (this.userRef != null) {
            this.userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Message msg = handler.obtainMessage();
                    user = snapshot.getValue(User.class);

                    msg.arg1 = READ_DONE;
                    if (user != null) {
                        if (user.getUsername().equals("")) {
                            msg.arg2 = READ_ERROR;
                        } else {
                            msg.arg2 = READ_OK;
                        }
                        msg.obj = (User) user;
                    } else {
                        msg.arg2 = NULL_USER;
                    }
                    handler.sendMessage(msg);
                    userRef.removeEventListener(this);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    userRef.removeEventListener(this);
                }
            });
        }
    }

    /**
     * The function is getting the given user and receiving an handler that uses this result
     *
     * @param handler Handler
     * @param id the other user's id to get
     */
    public void getOtherUser(Handler handler, String id) {

        if (this.usersRef != null) {
            this.usersRef.child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Message msg = handler.obtainMessage();
                    user = snapshot.getValue(User.class);

                    msg.arg1 = READ_DONE;
                    if (user != null) {
                        if (user.getUsername().equals("")) {
                            msg.arg2 = READ_ERROR;
                        } else {
                            msg.arg2 = READ_OK;
                        }
                        msg.obj = (User) user;
                    } else {
                        msg.arg2 = NULL_USER;
                    }
                    handler.sendMessage(msg);
                    usersRef.child(id).removeEventListener(this);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    usersRef.child(id).removeEventListener(this);
                }
            });
        }
    }


    /**
     * This function is getting the users
     *
     * @param handler Handler that uses the result
     * @param users   the list of the users to add to
     */
    public void getUsers(Handler handler, List<User> users) {
        usersEvent = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                users.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    User user = dataSnapshot.getValue(User.class);
                    if (!user.getKey().equals(currentUser.getUid())) {
                        users.add(user);
                    }
                }
                Message msg = handler.obtainMessage();
                msg.arg1 = USERS_UPDATED;
                handler.sendMessage(msg);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        };
        usersRef.addValueEventListener(usersEvent);
    }

    /**
     * This function is removing the event listener of the users event fetching
     */
    public void removeEventUsers(){
        if(this.usersEvent!=null) this.usersRef.removeEventListener(this.usersEvent);
    }

    /**
     * The function uploading a profile picture to firebase
     *
     * @param handler Handler
     * @param bmp     Bitmap
     *                the picture to upload to firebase
     */
    public void uploadPfp(Bitmap bmp, final Handler handler) {
        if (bmp != null) {
            //converting bitmap to bytes to upload
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            //ProgressDialog pd = CreateProgressDialog();
            storageReference = storage.getReference();
            final StorageReference ref = storageReference.child("images/" + uID + ".jpg");
            final Message msg = handler.obtainMessage();
            ref.putBytes(data)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //pd.dismiss();
                            msg.arg1 = DONE_UPLOAD_PIC;
                            msg.arg2 = SAVE_OK;
                            handler.sendMessage(msg);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //pd.dismiss();
                            msg.arg2 = SAVE_ERROR;
                            handler.sendMessage(msg);
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            //pd.setMessage("Loading..." + (int) progress + "%");
                        }
                    });
        }
    }

    /**
     * The function retrieving the profile picture of the user from firebase
     *
     * @param handler Handler
     * @param uID     String
     *                The user id of the user to get it's profile picture
     */
    public void retrievePfpUser(final Handler handler, String uID) {
        final Message msg = handler.obtainMessage();
        final long ONE_MEGABYTE = 1024 * 1024;
        final long ONE_K = 1024;
        final StorageReference picReference = storageReference.child("images/" + uID + ".jpg");
        picReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                pfp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                if (pfp == null) {
                    pfp = BitmapFactory.decodeResource(context.getResources(),
                            R.drawable.default_profile_picture);
                    msg.arg2 = NULL_RETRIEVED_PIC;
                } else msg.arg2 = OK_RETRIEVED_PIC;
                msg.obj = (Bitmap) pfp;
                msg.arg1 = DONE_RETRIEVED_PIC;
                if (handler != null)
                    handler.sendMessage(msg);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                pfp = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.default_profile_picture);
                msg.arg1 = DONE_RETRIEVED_PIC;
                msg.arg2 = ERROR_RETRIEVED_PIC;
                msg.obj = (Bitmap) pfp;
                handler.sendMessage(msg);
            }
        });
    }

    /**
     * The function receives a Message and a Handler and saving the Message
     *
     * @param message Message
     * @param handler Handler
     */
    public void saveMessage(Handler handler, com.example.conti.Model.Message message) {

        DatabaseReference.CompletionListener cl = new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                if(handler != null) {
                    Message msg = handler.obtainMessage();
                    msg.arg1 = SAVE_DONE;
                    if (error == null) {
                        msg.arg2 = SAVE_OK;
                    } else {
                        msg.arg2 = SAVE_ERROR;
                        msg.obj = error.getMessage();
                        Toast.makeText(context, error.getMessage() + "", Toast.LENGTH_SHORT).show();
                    }
                    handler.sendMessage(msg);
                }
            }
        };

        if(message instanceof TextMessage) {
            this.chatsRef.push().setValue((TextMessage) message, cl);
        }
        else if(message instanceof LocationMessage){
            this.chatsRef.push().setValue((LocationMessage) message, cl);
        }

    }

    /**
     * This function is getting the relevant messages of the current user
     *
     * @param handler Handler that uses the result
     * @param mChat The list of the chat to update
     * @param otherId the other user's id to receive the messages in the current chat
     * @param recyclerView rv to update to scroll always to bottom
     */
    public void getMsgs(Handler handler, List<com.example.conti.Model.Message> mChat, String otherId, RecyclerView recyclerView){
        chatEvent = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mChat.clear();
                for(DataSnapshot dataSnapshot: snapshot.getChildren()){
                    com.example.conti.Model.Message msg = dataSnapshot.getValue(com.example.conti.Model.Message.class);
                    //checking if the message is relevant
                    if(msg.getSenderId().equals(uID) && msg.getReceiverId().equals(otherId) ||
                            msg.getReceiverId().equals(uID) && msg.getSenderId().equals(otherId)) {
                        mChat.add(msg);
                    }
                }
                //recyclerView.scrollToPosition(recyclerView.getBottom()  - 1);
                Message msg = handler.obtainMessage();
                msg.arg1 = MSGS_UPDATED;
                handler.sendMessage(msg);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("sapir", "inside3");
            }
        };
        this.chatsRef.addValueEventListener(chatEvent);
    }

    /**
     * This function is removing the chat event listener
     */
    public void removeChatEvent(){
        this.chatsRef.removeEventListener(this.chatEvent);
    }


// --------------------------------------------------------------------------
//        Helpers
// ---------------------------------------------------------------------------

    /**
     * The function is creating a Progress Dialog
     *
     * @return the Progress Dialog
     */
    private ProgressDialog CreateProgressDialog() {
        ProgressDialog pd;
        pd = new ProgressDialog(context);
        pd.setTitle("In progress...");
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        return pd;
    }


    /**
     * This function makes a handler for getting the profile picture
     * @param iv ImageView to put the bitmap into
     * @return Handler.Callback
     */
    public Handler.Callback handleGetPfp(ImageView iv){
        return new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.DONE_RETRIEVED_PIC) {
                    if (msg.arg2 == FBDatabase.OK_RETRIEVED_PIC) {
                        Bitmap pfp = (Bitmap) msg.obj;
                        iv.setImageBitmap(pfp);
                    }
                }
                return true;
            }
        };
    }
}