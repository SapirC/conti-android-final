package com.example.conti;

import androidx.annotation.NonNull;
import androidx.annotation.PluralsRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 *  This Activity is the first to run
 *  if the user is already logged in, it will be forward to ChatsActivity
 *  otherwise he will have to login or sign in to the application
 *  @author Sapir Cohen
 *  @version  2021
 *
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth auth;

    private Button signInBtn, signUpBtn;
    private EditText emailEt, passwordEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        auth=FirebaseAuth.getInstance();

        signInBtn = (Button) findViewById(R.id.signInBtn);
        signInBtn.setOnClickListener(this);

        signUpBtn = (Button) findViewById(R.id.signUpBtn);
        signUpBtn.setOnClickListener(this);

        emailEt = (EditText) findViewById(R.id.emailEt);

        passwordEt = (EditText) findViewById(R.id.passwordEt);


    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(emailEt.getText()) || TextUtils.isEmpty(passwordEt.getText()))
        {
            Toast.makeText(LoginActivity.this, "Email or password are empty", Toast.LENGTH_SHORT).show();
        }
        else {
            ProgressDialog pd = CreateProgressDialog();
            if(v==signInBtn){
                Intent intent=new Intent(LoginActivity.this,
                        ChatsActivity.class);

                auth.signInWithEmailAndPassword(emailEt.getText().toString(),passwordEt.getText().toString()).addOnCompleteListener(
                        LoginActivity.this, signTask(intent, pd));
            }
            else if(v==signUpBtn){
                Intent intent=new Intent(LoginActivity.this,
                        SignupActivity.class);

                auth.createUserWithEmailAndPassword(emailEt.getText().toString(),passwordEt.getText().toString()).addOnCompleteListener(
                        LoginActivity.this, signTask(intent, pd));
            }
        }
    }

    private FirebaseAuth.AuthStateListener authStateListener=new
            FirebaseAuth.AuthStateListener() {
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user=firebaseAuth.getInstance().getCurrentUser();
                    if(user!=null)
                    {
                        Intent intent=new Intent(LoginActivity.this,ChatsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            };

    /**
     * Any time this Activity start/resume, we must check if the user is logged in or not.
     * To do so, we attached Listener to check if the user is already logged in
     */
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(authStateListener);
    }

    /**
     * Any time this Activity stop, we don't need the Listener,
     * so, we detach the Listener
     */
    protected void onStop() {
        super.onStop();
        if(authStateListener!=null)
            auth.removeAuthStateListener(authStateListener);
    }

    /**
     * This function creates a Progress Dialog
     *
     * @return Progress Dialog
     */
    private ProgressDialog CreateProgressDialog()
    {
        ProgressDialog pd;
        pd = new ProgressDialog(this);
        pd.setTitle("In progress...");
        pd.setMessage("Please Wait...");
        pd.setCancelable(true);
        pd.show();
        return pd;
    }

    /**
     * This function makes a sign task
     * @param i Intent
     *          the intent to move on after the sign up if completed
     * @param pd ProgressDialog
     *           the progress dialog used for the task
     * @return OnCompleteListener<AuthResult>
     */
    private OnCompleteListener<AuthResult> signTask(Intent i, ProgressDialog pd){
      return new OnCompleteListener<AuthResult>() {
          @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
              pd.dismiss();
              if(task.isSuccessful())
                {
                    startActivity(i);
                    finish();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Action failed ," +task.getException().getMessage(),
                            Toast.LENGTH_LONG).show();
                }
          }
        };
    }
}