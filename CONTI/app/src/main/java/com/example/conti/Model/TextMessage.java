package com.example.conti.Model;

/**
 * A Text type message
 */
public class TextMessage extends Message{

    public TextMessage(String senderId, String receiverId, String text) {
        super(senderId, receiverId, "txt" , (String) text);
    }
}
