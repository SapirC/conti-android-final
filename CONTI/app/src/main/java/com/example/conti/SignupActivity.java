package com.example.conti;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.conti.Model.User;

/**
 * Signup Activity to add details for a new user that has been logged in
 */
public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText usernameEt;
    private Button confirmBtn;
    private FBDatabase fb;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        usernameEt=(EditText)findViewById(R.id.usernameEt);

        confirmBtn=(Button)findViewById(R.id.confirmBtn);
        confirmBtn.setOnClickListener(this);

        fb=FBDatabase.getInstance(this);

        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.SAVE_DONE) {
                    if (msg.arg2 == FBDatabase.SAVE_OK) {
                        Intent intent = new Intent(SignupActivity.this, ChatsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                        Toast.makeText(SignupActivity.this, "Save Failed", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v==confirmBtn){

            if(usernameEt.getText().toString().length() != 0) {
                User user=new User(usernameEt.getText().toString());
                fb.saveUser(handler, user);
            }

            else {
                Toast.makeText(SignupActivity.this, "empty field!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
