package com.example.conti;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.conti.Model.User;

/**
 * A Foreground Service to close the app and logout moving to the killAppActivity,
 * After clicking the notification that is created here for the service
 */
public class NoteService extends Service {
    private User user;
    private FBDatabase fb;

    static final int REQ = 23;
    NotificationManager notificationManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String channelName = "CONTI";
        String channelDescription = "Conversation Time";
        String CHANNEL_ID = "channel";

        String title = "CONTI";
        String content = "CONTI is working.. tap to kill";

        Notification notification;
        NotificationChannel channel;

        Intent killAppIntent = new Intent(this,KillAppActivity.class);

        //passing the service intent to stop in the kill app activity
        killAppIntent.putExtra("service", intent);

        //not duplicated message
        killAppIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,REQ,killAppIntent,0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new
                    NotificationChannel(CHANNEL_ID, channelName, importance);
            channel.setDescription(channelDescription);
            // Register the channel with the system;
            // you can't change the importance
            // or other notification behaviors after this
            notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            notification = new Notification.Builder(
                    this, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher_conti)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .build();
        } else {
            notification = new Notification.Builder(
                    this)
                    .setSmallIcon(R.mipmap.ic_launcher_conti)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_LOW)
                    .build();
        }

        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager = (NotificationManager)
                getSystemService(
                        getApplicationContext().NOTIFICATION_SERVICE);

        notificationManager.notify(startId, notification);

        startForeground(startId, notification);

        return super.onStartCommand(intent ,flags ,startId);
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //setting that the user has logged out from the app
        fb = FBDatabase.getInstance(this);
        fb.logout();

        stopForeground(true);
    }
}
