package com.example.conti;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.conti.Adapter.UserAdapter;
import com.example.conti.Model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This Activity displays all the user you can chat with
 */
public class ChatsActivity extends AppCompatActivity implements View.OnClickListener {

    private FBDatabase fb;

    private User user;

    private Toolbar toolbar;

    private TextView usernameTv;
    private ImageView profileIv;

    private RecyclerView recyclerView;

    private UserAdapter userAdapter;
    private List<User> users;

    ArrayList<String> permissionsAll, permissionsNotGranted;
    static final int REQUEST_GROUP_PERMIT = 666;

    Intent intentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        //making a service that makes the user staying logged into the app
        intentService = new Intent(this, NoteService.class);
        startService(intentService);

        fb = FBDatabase.getInstance(this);

        //getting user information and checking if he had signed up
        fb.getUser(new Handler(handleNotSignedUp(this)));

        users = new ArrayList<User>();

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        usernameTv = (TextView) findViewById(R.id.usernameTv);

        profileIv = (ImageView) findViewById(R.id.profileIv);

        recyclerView = findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        userAdapter = new UserAdapter(this, users);
        recyclerView.setAdapter(userAdapter);

        updateUsers();

        makePermissions();
    }

    /**
     * This function requests relevant permissions from the user
     */
    public void makePermissions(){
        permissionsAll = new ArrayList<String>();
        permissionsNotGranted = new ArrayList<String>();

        //adding required permissions
        permissionsAll.add(Manifest.permission.CAMERA);
        permissionsAll.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissionsAll.add(Manifest.permission.ACCESS_FINE_LOCATION);

        //requesting permissions
        for (int i = 0; i < permissionsAll.size(); i++) {
            if (ContextCompat.checkSelfPermission(this,
                    permissionsAll.get(i)) !=
                    PackageManager.PERMISSION_GRANTED) {
                permissionsNotGranted.add(permissionsAll.get(i));
            }
        }

        requestGroupPermissions(permissionsNotGranted);
    }

    /**
     * This function makes the request to the user for every permission
     * @param permissions ArrayList<String> The permissions to request
     */
    public void requestGroupPermissions(ArrayList<String> permissions) {
        if (permissions != null && permissions.size() > 0) {
            String[] permissionGroup = new String[permissions.size()];
            permissions.toArray(permissionGroup);
            ActivityCompat.requestPermissions(this,
                    permissionGroup, REQUEST_GROUP_PERMIT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_GROUP_PERMIT) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] !=
                        PackageManager.PERMISSION_GRANTED) {

                }
            }
        }
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * This function is updating the users array using the database
     */
    private void updateUsers(){
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.arg1 == FBDatabase.USERS_UPDATED) {
                    userAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });
        fb.getUsers(handler, users);
    }


    /**
     * This function makes a handler of a check for a user that didn't add any details
     * @param context Context
     * @return Handler.Callback
     */
    private Handler.Callback handleNotSignedUp(Context context){
        return new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {

                if (msg.arg1 == FBDatabase.READ_DONE) {
                    Intent intent = new Intent();
                    //the user didn't signed up
                    if (msg.arg2 == FBDatabase.READ_ERROR || msg.arg2 == FBDatabase.NULL_USER) {
                        intent = new Intent(context, SignupActivity.class);
                        startActivity(intent);
                        fb.removeEventUsers();
                        finish();
                    }
                    //the user signed up
                    else if(msg.arg2 == FBDatabase.READ_OK) {
                        user  = (User) msg.obj;
                        usernameTv.setText(user.getUsername());
                        //getting user's profile picture
                        fb.retrievePfpUser(new Handler(fb.handleGetPfp(profileIv)), user.getKey());

                        //setting that the user has logged into the app
                        user.setLogged(true);
                        fb.saveUser(null, user);
                    }
                }
                return true;
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chats, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                fb.removeEventUsers();
                fb.logout();

                finish();
                break;
            case R.id.settings:
                fb.removeEventUsers();
                startActivity(new Intent(this, SettingsActivity.class));
                finish();
                break;
        }
        return true;
    }
}