package com.example.conti.Model;

import java.io.Serializable;

/**
 * User class that has all the relevant details about the user
 */
public class User {
    private String username;
    private String key;
    private Boolean logged;

    public User(User user) {
        this.username = user.getUsername();
        this.key = user.getKey();
        this.logged = user.getLogged();
    }

    public User(String username) {
        this.username = username;
        this.key = "";
        this.logged = true;
    }

    public User(){
        this.username = this.key = "";
        this.logged = false;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getLogged() {
        return logged;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }
}
