package com.example.conti.Model;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.conti.R;

import java.io.File;


/**
 * A picture Dialog class for displaying this dialog,
 * choosing a picture from gallery or taking a picture with the camera
 */
public class PictureDialog extends Dialog implements View.OnClickListener {
    private Dialog dialog;
    private Activity act;

    private TextView cancelTv;
    private ImageView cameraIv, galleryIv;

    private final static int TAKE_PICTURE=900,PICK_PICTURE=800;

    public PictureDialog(Activity act){
        super(act);
        this.act = act;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.picture_dialog);
        cameraIv = (ImageView) findViewById(R.id.cameraIv);
        galleryIv = (ImageView) findViewById(R.id.galleryIv);
        cancelTv = (TextView) findViewById(R.id.cancelTv);
        cameraIv.setOnClickListener(this);
        galleryIv.setOnClickListener(this);
        cancelTv.setOnClickListener(this);

        this.dialog = new Dialog(act);
        this.dialog.setContentView(R.layout.picture_dialog);
        this.dialog.setCancelable(true);
    }

    @Override
    public void onClick(View v) {
        if(v== cameraIv) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            this.act.startActivityForResult(cameraIntent, TAKE_PICTURE);
            dismiss();
        }
        else if(v== galleryIv) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            File picDir= Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            String picDirPath=picDir.getPath();
            Uri uData= Uri.parse(picDirPath);
            galleryIntent.setDataAndType(uData,"image/*");
            this.act.startActivityForResult(galleryIntent, PICK_PICTURE);
            dismiss();
        }
        else if(v==cancelTv) {
            dismiss();
        }
    }

    public Bitmap onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bmp = null;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TAKE_PICTURE:
                    bmp = (Bitmap) data.getExtras().get("data");
                    break;
                case PICK_PICTURE:
                    Uri URI = data.getData();
                    String[] FILE = {MediaStore.Images.Media.DATA};
                    Cursor cursor = this.act.getContentResolver().query(URI,
                            FILE, null, null, null);
                    cursor.moveToFirst();
                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    int columnIndex = cursor.getColumnIndex(FILE[0]);
                    String ImageDecode = cursor.getString(columnIndex);
                    cursor.close();
                    options.inSampleSize = 4;
                    bmp = BitmapFactory.decodeFile(ImageDecode, options);
                    break;
            }
        }
        return bmp;
    }
}




