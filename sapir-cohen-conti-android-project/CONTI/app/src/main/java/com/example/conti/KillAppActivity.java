package com.example.conti;

import androidx.appcompat.app.AppCompatActivity;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * This activity runs after the message of the service that works,
 * while the app works,
 * The role of this activity is to kill the application,
 * and make a logout for the user form the database
 */
public class KillAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if(getIntent().getExtras() != null) {
            Intent serviceIntent = getIntent().getParcelableExtra("service");
            stopService(serviceIntent);

            //clearing notifications
            NotificationManager notificationManager = (NotificationManager)getSystemService(this.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        }

        finishAffinity();
    }
}