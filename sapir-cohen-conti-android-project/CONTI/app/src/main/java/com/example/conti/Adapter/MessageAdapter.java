package com.example.conti.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import androidx.core.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.conti.FBDatabase;
import com.example.conti.MessageActivity;
import com.example.conti.Model.Message;
import com.example.conti.Model.TextMessage;
import com.example.conti.Model.User;
import com.example.conti.R;
import com.google.android.gms.maps.GoogleMapOptions;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * A class Adapter for the messages array that displays how every message looks like
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    public static final int LOCATION_SENT = 77;

    public static final int MSG_TXT_LEFT = 1;
    public static final int MSG_TXT_RIGHT = 2;
    public static final int MSG_MAP_LEFT = 3;
    public static final int MSG_MAP_RIGHT = 4;

    private FBDatabase fb;
    private User user;
    private User otherUser;
    private Context context;
    private List<Message> mChat;
    private Message msg;

    private Handler handlerGoogleMap;

    public MessageAdapter(Context context, List<Message> mChat, User user, User otherUser, Handler handlerGoogleMap) {
        this.context = context;
        this.mChat = mChat;
        this.user = user;
        this.otherUser = otherUser;
        this.fb = FBDatabase.getInstance(this.context);
        this.handlerGoogleMap = handlerGoogleMap;
    }

    @NonNull
    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType){
            case MSG_TXT_RIGHT:
                view = LayoutInflater.from(context).inflate(R.layout.msg_txt_right, parent, false);
                break;
            case MSG_TXT_LEFT:
                view = LayoutInflater.from(context).inflate(R.layout.msg_txt_left, parent, false);
                break;
            case MSG_MAP_RIGHT:
                view = LayoutInflater.from(context).inflate(R.layout.msg_map_right, parent, false);
                break;
            case MSG_MAP_LEFT:
                view = LayoutInflater.from(context).inflate(R.layout.msg_map_left, parent, false);
                break;
            default:
                break;
        }
        return new MessageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.ViewHolder holder, int position) {
        msg = mChat.get(position);
        if( msg != null){
            if(msg.getType().equals("txt")){
                holder.tvMsg.setText((String)msg.getData());
            }
            else{
                holder.tvTime.setText(msg.getDateTime());

                //getting the google map url that will display the location in a map
                String getMapURL = "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=270x150&markers=size:mid|color:red|"
                        + ((HashMap<String, Double>)msg.getData()).get("latitude")
                        + ","
                        + ((HashMap<String, Double>)msg.getData()).get("longitude")
                        + "&sensor=false"
                        + "&key="
                        + context.getString(R.string.google_maps_key);

                //displaying the coordinates
                holder.locTv.setText(((HashMap<String, Double>)msg.getData()).get("latitude") + ", " +
                                        ((HashMap<String, Double>)msg.getData()).get("longitude"));

                //checking if location picture did not load yet
                if(holder.mapIv.getDrawable() != context.getResources().getDrawable(R.drawable.default_profile_picture)){
                    //getting from the url the image of the map to display in the message
                    new DownLoadImageTask(holder.mapIv).execute(getMapURL);
                }

            }
        }
    }

    /**
     This class Executes the task with the specified parameters.
     */
    private class DownLoadImageTask extends AsyncTask<String,Void, Bitmap> {
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView){
            this.imageView = imageView;
        }

        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();
                /*
                    decodeStream(InputStream is)
                        Decode an input stream into a bitmap.
                 */
                logo = BitmapFactory.decodeStream(is);
            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        /*
            onPostExecute(Result result)
                Runs on the UI thread after doInBackground(Params...).
         */
        protected void onPostExecute(Bitmap result){
            imageView.setImageBitmap(result);
        }
    }

    @Override
    public int getItemCount() {
        return mChat.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvMsg;
        public TextView tvTime;
        public TextView locTv;

        public ImageView mapIv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mapIv = (ImageView) itemView.findViewById(R.id.mapIv);

            locTv = (TextView) itemView.findViewById(R.id.locTv);

            tvTime = (TextView) itemView.findViewById(R.id.timeTvm);

            tvMsg = (TextView) itemView.findViewById(R.id.tvMsg);

        }
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);


    }

    @Override
    public int getItemViewType(int position) {
        if(user.getKey().equals(mChat.get(position).getSenderId())){
            if(mChat.get(position).getType().equals("txt")){
                return MSG_TXT_RIGHT;
            }
            else if(mChat.get(position).getType().equals("map")){
                return MSG_MAP_RIGHT;
            }
        }
        else{
            if(mChat.get(position).getType().equals("txt")){
                return MSG_TXT_LEFT;
            }
            else if(mChat.get(position).getType().equals("map")){
                return MSG_MAP_LEFT;
            }
        }
        return 0;
    }
}

