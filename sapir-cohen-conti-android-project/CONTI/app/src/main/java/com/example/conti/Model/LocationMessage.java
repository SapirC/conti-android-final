package com.example.conti.Model;

import android.location.Location;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * A location type message
 */
public class LocationMessage extends Message{

    public LocationMessage(String senderId, String receiverId, Location location) {
        super(senderId, receiverId, "map", (LatLng) new LatLng(location.getLatitude(), location.getLongitude()));
    }
}
