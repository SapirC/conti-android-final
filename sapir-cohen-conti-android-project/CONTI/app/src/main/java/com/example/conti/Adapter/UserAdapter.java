package com.example.conti.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.conti.FBDatabase;
import com.example.conti.MessageActivity;
import com.example.conti.Model.User;
import com.example.conti.R;

import java.util.List;

/**
 * A class Adapter for the users array that displays every user
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder>{

    private FBDatabase fb;
    private Context context;
    private List<User> users;

    public UserAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
        this.fb = FBDatabase.getInstance(this.context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false);
        return new UserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = users.get(position);
        holder.username.setText(user.getUsername());

        if(!user.getLogged()) holder.online.setVisibility(View.INVISIBLE);

        holder.pfp.setBackgroundResource(R.drawable.default_profile_picture);
        fb.retrievePfpUser(new Handler(fb.handleGetPfp(holder.pfp)),user.getKey());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MessageActivity.class);
                intent.putExtra("userKey", user.getKey());
                context.startActivity(intent);
                fb.removeEventUsers();
                //((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView username;
        public ImageView pfp;
        public ImageView online;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.usernameTv);
            pfp = itemView.findViewById(R.id.profileIv);
            online = itemView.findViewById(R.id.onlineIV);

        }

    }
}
